# README #

### Instruções para execução  ###

* Task 1 Versão com Spark SQL
    * **Main** class:  app.Task1SparkSQL
    
    * Fornecer como argumento o caminho da pasta resourse no ambiente    
        Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task1/resource/
        
    * Arquivo a ser processado: O arquivo deve estar dentro da pasta Input dentro do diretório resource  
        Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task1/resource/Input/
        
    * Após o processamento o arquivo csv será gravado na pasta Output dentro da pasta resource   
        Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task1/resource/Output/ 
        
 * Task 1 Versão com RDD
     * **Main** class: app.Task1RDD
     
     * Fornecer como argumento o caminho da pasta resourse no ambiente    
         Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task1/resource/
         
     * Arquivo a ser processado: O arquivo deve estar dentro da pasta Input dentro do diretório resource  
         Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task1/resource/Input/
         
     * Após o processamento o arquivo csv será gravado na pasta Output dentro da pasta resource   
         Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task1/resource/Output/        
* Task 2
    * **Main** class:  app.Task2
    
    * Fornecer como argumento o caminho da pasta resourse no ambiente    
        Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task2/resource/
        
    * Arquivo a ser processado: O arquivo deve estar dentro da pasta Input dentro do diretório resource  
        Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task2/resource/Input/
        
    * Após o processamento o arquivo csv será gravado na pasta Output dentro da pasta resource   
        Ex: /Users/paulo/desenvolvimento/desafioluizalabs/Task2/resource/Output/ 

  

### Observações ###

* Para um melhor visualização no excel o separador do arquivo da task 2 foi colocado como ";"
* Considerei como últimos 3 anos da black friday 2018/2019/2020



### Paulo Almeida
### pl_alves@yahoo.com.br
