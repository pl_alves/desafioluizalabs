package app

import java.sql.Timestamp
import java.util.Date
import Enuns.enums
import domain.{Report, WordSize}
import org.apache.spark.SparkContext
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}

object Task1SparkSQL {


  def main(args : Array[String]) = {

    val spark = SparkSession.builder()
      .appName("Task1")
      .getOrCreate()

    val sc = SparkContext.getOrCreate()
    sc.setLogLevel("ERROR")
    import spark.implicits._

    val path = args(0)
    val readPath = path+"Input"
    val writePath = path+"Output"

    println("Diretório de leitura " + readPath)
    println("Diretório de saída "+ writePath)

    println("INICIO DO PROCESSAMENTO   " + new Timestamp(new Date().getTime))

    // ABRE O ARQUIVO E QUEBRA AS LINHAS EM PALAVRAS
    val wordsFromFile: Dataset[String] = spark.read.textFile(readPath).flatMap(f => f.split(" "))

    // PADRONIZA AS PALAVRAS REMOVENDO PONTUAÇÕES E OUTROS CARACTERES E DEIXANDO TODAS EM LOWERCASE
    // ADICIONO O TAMANHO DE CADA PALAVRA
    // E ELIMINO AS LINHAS VAZIAS
    val wordSize = wordsFromFile.select(
      lower(regexp_replace(col("value"),"[^A-Za-z0-9]","")).as("word"))
      .withColumn("size", length(col("word")))
      .filter($"size" > 0)
      .as[WordSize]

    // VERIFICO SE A PALAVRA POSSUI MAIS QUE 10 CARACTERES.
      // - SE POSSUIR SUBSTITUO PELA PALAVRA DEFAULT
      // - CASO CONTRÁRIO MANTENHO A PALAVRA ORIGINAL
    // AGRUPO AS PALAVRAS IGUAIS
    // CONTO QUANTAS PALAVRAS IGUAIS EXISTEM
    val countWordFromFile = wordSize.select(
      when(col("size") > enums.parameterSizeWord ,enums.defaultWord).otherwise(col("word")).as("word"))
      .groupBy("word")
      .agg(count("word").alias("count")).as[Report].cache()

    //countWordFromFile.show(truncate = false, numRows = 10)

    ///coalesce para agrupar os dados em unica partição para gerar um único csv no diretorio de saida
    countWordFromFile.coalesce(1).write.format("csv").option("header","true").mode(SaveMode.Overwrite).save(writePath)
    println("FIM DO PROCESSAMENTO      " + new Timestamp(new Date().getTime))

  }
}
