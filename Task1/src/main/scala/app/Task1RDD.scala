package app

import java.sql.Timestamp
import java.util.Date
import Enuns.enums
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SaveMode, SparkSession}


object Task1RDD {
  def main(args : Array[String]) = {

    val spark = SparkSession.builder()
      .appName("Task1")
      .getOrCreate()

    val sc = SparkContext.getOrCreate()
    sc.setLogLevel("ERROR")
    import spark.implicits._

    val path = args(0)
    val readPath = path+"Input"
    val writePath = path+"Output"

    println("Diretório de leitura " + readPath)
    println("Diretório de saída "+ writePath)

    println("INICIO DO PROCESSAMENTO   " + new Timestamp(new Date().getTime))

    /// ABRO O ARQUIVO E QUEBRO AS LINHAS EM PALAVRAS PELO SEPARADO DE ESPCAÇO
    val words: RDD[(String, Int)] = sc.textFile(readPath).flatMap(line => line.split(" "))
      .filter(f => f.size > 0) //REMOVO AS LINHAS EM BRANCO
      .map(word => (this.normalizeWord(word), 1) ) ///PADRONIZO AS PALAVRAS
      .reduceByKey(_ + _) //REALIZO A CONTA DA QUANTIDADE DE PALAVRAS

    //GRAVO O RESULTADO EM UM ARQUIVO
    words.toDS().coalesce(1).write.format("csv").option("header","true").mode(SaveMode.Overwrite).save(writePath)

    println("FIM DO PROCESSAMENTO      " + new Timestamp(new Date().getTime))

  }

  def normalizeWord(word : String): String = {
    // REMOVO QUALQUER CARACTER DE PONTUAÇÃO E DEIXO TUDO EM CAIXA BAIXA
    val normalizeWord: String = word.toLowerCase().replaceAll("[^A-Za-z0-9]","")
    // SE O TAMANHO DA PALAVRA FOR MAIOR QUE 10 TROCO PELA PALAVRA PELA PADRÃO MAIORES QUE 10, CASO CONTRÁRIO MANTENHO A PALAVRA ORIGINAL
    if (normalizeWord.size > enums.parameterSizeWord) enums.defaultWord else normalizeWord
  }
}
