package app

import java.sql.Timestamp
import java.util.Date
import java.util.Calendar
import domain.{Order, Orders, OrdersList}
import org.apache.spark.sql.{ DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.SparkContext
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.expressions.Window


object Task2 {

  /// PARÂMETROS PARA OTIMIZAÇÃO DE PERFORMANCE
  // UTILIZAR PREFERENCIALMENTE A ESTRATEGIA Sort Merge Join PARA REDUZIR A QUANTIDADE DE SHUFFLE ENTRE OS WORKERS
  val spark = SparkSession.builder().appName("Task2")
    .config("spark.sql.codegen.wholeStage", true)
    .config("spark.sql.join.preferSortMergeJoin", true)
    .config("spark.sql.autoBroadcastJoinThreshold", -1)
    .config("spark.sql.defaultSizeInBytes", 100000)
    .config("spark.sql.shuffle.partitions", 20)
    .getOrCreate()

  val sc = SparkContext.getOrCreate()
  sc.setLogLevel("ERROR")
  import spark.implicits._

  def main(args : Array[String]) = {

    val path = args(0)
    val readPath = path+"Input"
    val writePath = path+"Output"

    println("Diretório de leitura " + readPath)
    println("Diretório de saída "+ writePath)

    println("INICIO DO PROCESSAMENTO   " + new Timestamp(new Date().getTime))

    // LEITURA DO ARQUIVO DE PEDIDOS
    val schema = "codigo_pedido STRING ,codigo_cliente STRING ,data_nascimento_cliente STRING ,data_pedido STRING"
    val ordersFromFile = spark.read
      .format("csv")
      .option("header","true")
      .schema(schema)
      .option("dateFormat","yyyy-MM-dd")
      .load(readPath)

    //FORMATO A DATA DO PEDIDO DE EPOCH PARA YYYY-MM-DD
    val ordersLoadedDF: Dataset[Row] = ordersFromFile.withColumn("data_pedido", from_unixtime(col("data_pedido") , "yyyy-MM-dd")).cache()

    //RETORNA OS DIAS DA BLACK FRIDAY DOS 3 ÚLTIMOS ANOS
    val blackFridayDays = this.getBFDays(Calendar.getInstance.get(Calendar.YEAR) - 3)

    // FILTRO OS PEDIDOS VALIDOS  => Pedidos realizados na black friday dos últimos 3 anos
    // Forço Broadcast Join. Como o DF das datas da black friday é pequeno eu distribuo o DF para todos os workers evitando assim shuffle
    val validOrders = ordersLoadedDF.join(broadcast(blackFridayDays), ordersLoadedDF("data_pedido") === blackFridayDays("black_friday_days"), "inner").distinct().cache()

    // UNIVERSO DE CLIENTES MAIORES DE 30 ANOS COM MAIS DE 2 PEDIDOS NOS DIAS DA BLACK FIRDAY
    //  CALCULO A IDADE DOS CLIENTES
    //  FILTRO APENAS OS QUE SÃO MAIORES DE 30 ANOS
    //  CLIENTES COM MAIS DE 2 PEDIDOS NA BLACK FRIDAY
    val validClients: DataFrame = validOrders.select(col("codigo_cliente"), col("data_nascimento_cliente"), col("codigo_pedido"))
      .withColumn("idade",
        months_between(current_date(), col("data_nascimento_cliente"))
          .divide(12).cast("int"))
      .groupBy(col("codigo_cliente"), col("idade")).agg(count("codigo_pedido").as("numero_pedidos"))
      .filter(col("idade") < 30 )
      .filter(col("numero_pedidos") > 2 ).cache()


    /// CONSULTO NA LISTA DE PEDIDOS FORNECIDA TODOS OS PEDIDOS DOS CLIENTE VALIDOS(MAIORES DE 30 ANOS E COM MAIS DE 2 PEDIDOS NO PERÍODO)
    val allOrdersFromValidClients = ordersLoadedDF.as("orders").join(validClients.as("clients"), validClients("codigo_cliente") === ordersLoadedDF("codigo_cliente"), "inner" )
        .select(
          col("clients.codigo_cliente").cast("String"),
          col("orders.codigo_pedido").cast("String"),
          col("orders.data_pedido").cast("String")
          ).as[Order].cache()


    ////// TRANSFORMAÇÃO DA LISTA DE PEDIDOS PARA EXIBIÇÃO FORMATADA
    // TRANSFORMO TODOS OS PEDIDOS DOS CLIENTES VALIDOS EM UMA LISTA
    val allValidOrdersList: List[Order] = allOrdersFromValidClients.collect().toList
    //  AGRUPO OS PEDIDOS PELO CLIENTE
    val allOrderGrouped: Map[String, List[Order]] = allValidOrdersList.groupBy(_.codigo_cliente)
    // APÓS O groupBY FORMATO PARA A ESTRUTURA ESPERADA NA SAIDA => CLIENTE LISTA [PEDIDOS, DATA-PEDIDO]
    val ordersList :List[OrdersList] = allOrderGrouped.map(m => {
      val pedidos: List[Orders] = m._2.map(p => Orders(p.codigo_pedido,p.data_pedido))
      OrdersList(m._1, pedidos)
    }).toList

    // TRANFORMO A INFORMAÇÃO NOVAMENTE PARA DataSet
    val listOrders: Dataset[OrdersList] = ordersList.toDS().cache()

    val report = validClients.as("clients").join(listOrders.as("orders"), validClients("codigo_cliente") === listOrders("codigo_cliente"), "inner" )
        .select("clients.codigo_cliente", "clients.idade", "clients.numero_pedidos", "orders.lista_pedidos")
        .withColumn("lista_pedidos",col("lista_pedidos").cast("String")   ).cache()

    //println("SIZE " +report.count())
    //report.show(truncate = false, numRows = 3)


    report.coalesce(1).write.format("csv").option("header","true").option("delimiter", ";").mode(SaveMode.Overwrite).save(writePath)

    println("FIM                       " + new Timestamp(new Date().getTime))


    //Thread.sleep(100000000)
  }

  // FUNÇÃO RESPONSAVEL POR RETORNAR OS DIAS DA BLACK FRIDAY DO ANO X ATÉ O ATUAL
  def getBFDays(minimumYear : Int) = {
    // DIA DA BLACK FRIDAY  =>  dia seguinte à quarta quinta-feira do mês de novembro.

    // RETORNA OS 3 ANOS ANTERIORES
    val allYears = (minimumYear to (Calendar.getInstance.get(Calendar.YEAR) -1)).toList

    // PARA TODOS OS ANOS RETORNA TODOS OS DIAS DO MES DE NOVEMBRO
    val allDaysFromTheMonth  =  allYears.flatMap(year => {
      this.getAllDaysOfTheMonth(year,11)
    }).toDS().cache()

    // PARA TODOS OS DIAS DO MÊS DE NOVEMBRO SELECIONO OS DIAS QUE SÃO QUINTA FEIRA
    val allThursdays: Dataset[Row] =  allDaysFromTheMonth.withColumn("data", to_date(col("value"), "yyyy-MM-dd" ))
      .withColumn("dia_semana", date_format(col("data"), "EEEE")) // ENCONTRO O DIA DA SEMANA
      .withColumn("ano_mes", date_format(col("data"), "MM-yyyy")) // INCLUO UMA COLUNA COM O MÊS E ANO
      .filter(col("dia_semana") === "Thursday") // FILTRO TODOS OS DIAS QUE SÃO QUINTA-FEIRA
      .orderBy(col("data"))

    // DECLARO QUE A PARTIÇÃO DEVE SER FEITA PELO MÊS/ANO
    val windowPartition  = Window.partitionBy("ano_mes").orderBy("data")

    // CLASSIFICO TODAS AS QUINTAS FEIRAS POR MÊS/ANO
    // SELECIONO A QUARTA QUINTA FEIRA DE CADA MÊS/ANO
    // ADICIONO UM DIA PARA RETORNAR O DIA DA BLACK FRIDAY
    val blackFridayDays: DataFrame =   allThursdays.withColumn("row_number",row_number.over(windowPartition))
      .filter(col("row_number") === 4) // FILTRO A QUARTA QUINTA FEIRA DO MÊS/ANO
      .select(date_add(col("data"),1).as("black_friday_days") ) //ADICIONO + 1 DIA PARA RETORNAR O DIA DA BLACK FRIDAY

    blackFridayDays
  }

  /// FUNÇÃO RESPONSÁVEL POR RETORNAR TODOS OS DIAS DE UM DETERMINADO MÊS E ANO
  def getAllDaysOfTheMonth(year: Int , month:Int): List[String] = {
    // RETORNA A QUANTIDADE DE DIAS DE UM DETERMINADO MÊS/ANO
    val lengthOfMonth = java.time.YearMonth.of(year.toInt,month.toInt).lengthOfMonth
    // LISTA DE DIAS DO MÊS
    val daysOfMonth = (1 to lengthOfMonth).toList
    // DOS DIAS ENCONTRADOS FORMATA PARA O PADRÃO YYYY-MM-DD
    val listOfDays = daysOfMonth.map(day => {
      java.time.LocalDate.of(year,month,day).toString
    })

    listOfDays

  }
}
